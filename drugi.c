#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define MAX 100000

int var = 0;

void *povecaj_var_funkcija(void *pok){
    var++;
    pthread_exit(pok);
}

int main(int argc, char **argv){
    if (argc>2) {
        fprintf(stderr, "Unjeli ste previše parametara!\n");
        exit(EXIT_FAILURE);
    }
    if (argc<2) {
        fprintf(stderr, "Unjeli ste premalo parametara!\n");
        exit(EXIT_FAILURE);
    }


    int d, status;
    char *end;
    int broj_threadova =  (int)strtol(argv[1], &end, 10);
    pthread_t thread[MAX];
    for(d = 0; d < broj_threadova; d++){
        status = pthread_create(&thread[d], NULL, povecaj_var_funkcija, (void *) d);
        if(status){
            fprintf(stderr, "GRESKA prilikom izlaska iz pthread_create() je %d\n", status);
            exit(EXIT_FAILURE);
        }
    }

    fprintf(stdout, "Iznos varijable var: %d\n", var);

    return 0;
}
