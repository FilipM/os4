#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define BROJ_MJESTA 10
#define BROJ_THREADOVA 100

int avionska_mjesta[BROJ_MJESTA][BROJ_MJESTA];

void besmislena_funkcija_za_besmisleni_zahtjev(){
    sleep((uint)rand() % 3);
}

void *osiguraj_mjesto(void *pok){
    besmislena_funkcija_za_besmisleni_zahtjev();
    int broj_mjesta_x = rand() % 10;
    int broj_mjesta_y = rand() % 10;
    int odabrao = rand() % 100;
    if(odabrao <= 75){
        sleep((uint)rand() % 2);
        avionska_mjesta[broj_mjesta_x][broj_mjesta_y] = 1;
    }

    pthread_exit(pok);
}

int main(int argc, char **argv){
    int i, j, status;
    srandom((uint)time(NULL));
    pthread_t korisnici[BROJ_THREADOVA];

    for(i = 0; i < BROJ_MJESTA; i++){
        for(j = 0; j < BROJ_MJESTA; j++){
            avionska_mjesta[i][j] = 0;
        }
    }

	int broj_sretnika = (int)rand() % 100;

    for(i = 0; i < BROJ_THREADOVA; i++){
		if(i == broj_sretnika){
			sleep(1);
		}
        sleep((uint)rand() % 5);
        status = pthread_create(&korisnici[i], NULL, osiguraj_mjesto,
               (void *) i);
        if(status){
            fprintf(stderr, "GRESKA prilikom izlaska iz pthread_create() je %d\n", status);
            exit(EXIT_FAILURE);
        }
    }

    int br_zauzetih = 0, br_slobodnih = 0;
    fprintf(stdout, "Mjesta u avionu:\n");
    for(i = 0; i < BROJ_MJESTA; i++){
        for(j = 0; j < BROJ_MJESTA; j++){
            fprintf(stdout, "[%d] ", avionska_mjesta[i][j]);
            avionska_mjesta[i][j] == 1 ? br_zauzetih++ : br_slobodnih++;
        }
        fprintf(stdout, "\n");
    }
    fprintf(stdout, "Broj zauzetih mjesta: %d\nBroj slobodnih mjesta: %d\n",
            br_zauzetih, br_slobodnih);

    return 0;
}
