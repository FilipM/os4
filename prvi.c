#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int var = 0;

int main(int argc, char **argv){
    if(argc > 2){
        fprintf(stderr, "Unjeli ste previše parametara!\n");
        exit(EXIT_FAILURE);
    }
    if(argc < 2){
        fprintf(stderr, "Unjeli ste premalo parametara!\n");
        exit(EXIT_FAILURE);
    }

    char *end;
    int i, broj_procesa = (int)strtol(argv[1], &end, 10);
    pid_t childPid;
    for(i = 0; i < broj_procesa; i++){
        childPid = fork();
        switch(childPid){
        case -1:
            fprintf(stderr, "Nema resursa: %d\n", i);
            return 1;
        case 0: // Child
            var++;
            exit(0);
        default: // Parent
            wait(NULL);
        }
    }


    fprintf(stdout, "Varijabla var: %d\n", var);

    return 0;
}